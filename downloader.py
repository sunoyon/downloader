import feedparser
import urllib2, os
import logging
import threading
import time
import Queue
import sys

exitFlag = 0
 
class ParsedItem:
    'Class for Parsed Item . Each Item Having FILE URL'
    parsedItemCount = 0
      
    def __init__(self, itemTitle, itemURL):
        self.itemTitle = itemTitle
        self.itemURL = itemURL
        ParsedItem.parsedItemCount += 1
        
class FileDownloader(threading.Thread):
    'Class for downloading File from the URL'
    loopCount = -1
    def __init__(self, threadID, theadName, itemQueue):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.threadName = theadName
        # an instance of ParsedItem
        self.itemQueue = itemQueue
        self.isRunning = True
            
    def run(self):
        while not exitFlag:
            itemQueueLock.acquire()
            if not itemQueue.empty():
                item = self.itemQueue.get()
                itemQueueLock.release()
                logging.info('Start Downloading: %s' % item.itemTitle)
                fileName = item.itemURL.split('/')[-1]
                isPaused = self.downloadFile(self.threadName, fileName, item.itemURL)
                if isPaused == 1:
                    self.itemQueue.put(item)
            else:
                itemQueueLock.release()
                self.isRunning = False        
 
    def downloadFile(self, threadName, fileName, url):
    
        req = urllib2.Request(url)
        existFileLen = 0
        print threadName + '- URL: ' + url
        # check whether the file is partially/full downloaded
        if os.path.exists(fileName):
            f = open(fileName, "ab")
            existFileLen = os.path.getsize(fileName)
            req.headers['Range'] = 'bytes=%s-' % existFileLen
            u = urllib2.urlopen(req)
        else:
            f = open(fileName, 'wb')
            u = urllib2.urlopen(req)
            
        meta = u.info()
        fileSize = int(meta.getheaders("Content-Length")[0])
              
        if existFileLen == fileSize:
            logging.info('%s is already downloaded!' % fileName ) 
        else:  
            fileSizeDl = 0
            block_sz = 8192
            it = 0
            while True:
                buff = u.read(block_sz)
                if not buff:
                    break          
                fileSizeDl += len(buff)
                f.write(buff)
                status = r"%s: %s  [%3.2f%%]" % (threadName, fileName, (fileSizeDl + existFileLen) * 100. / (fileSize + existFileLen))
                logging.info(status)
                time.sleep(1)
#               Download Pause
#                 if self.isRunning == False:
                if it == FileDownloader.loopCount:
                    u.close()
                    f.close()
                    status = fileName + ' download is paused'
                    logging.info(status)
                    return 1
                it += 1
            status = fileName + ' downloading completed!'
            logging.info(status)
        u.close()
        f.close()
        return 0


  
logging.basicConfig(filename='downloader.log',level=logging.DEBUG)
 
rssFeedUrl = 'https://dl.dropboxusercontent.com/u/6160850/downloads.rss'
if len(sys.argv) > 1:
    rssFeedUrl = sys.argv[1]
    if len(sys.argv) > 2:
        FileDownloader.loopCount = int(sys.argv[2])       
    
feedParserObj = feedparser.parse(rssFeedUrl)
parsedItemList = []
  
logging.info('Getting all download links:')
for i in range(0, len(feedParserObj['entries'])):
    tmpItem = ParsedItem(feedParserObj['entries'][i].title, feedParserObj['entries'][i].link)
    parsedItemList.append(tmpItem)
    logging.info('%s -> %s' % (i + 1, tmpItem.itemTitle))

itemQueueLock = threading.Lock()
itemQueue = Queue.Queue(ParsedItem.parsedItemCount)
itemQueueLock.acquire()
for i in parsedItemList:
    itemQueue.put(i)
itemQueueLock.release() 

   
threads = []   
# Create new threads
threadsCount = 0
while threadsCount < 2:
    filedwn = FileDownloader( threadsCount + 1, "Thread-" + str(threadsCount + 1), itemQueue)
    filedwn.start()
    threads.append(filedwn)
    threadsCount += 1
    
# Wait for queue to empty
while not itemQueue.empty():
    pass
# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Program is terminated!"
